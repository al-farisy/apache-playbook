this is a README

What is Apache Web Server?

Apache or Apache HTTP server is a free and open source web server, developed and maintained by the Apache Software Foundation. Its popularity can be judged by the fact that around 46% of the websites worldwide are powered by Apache. Apache allows website developers to serve their content over the web. It serves as a delivery man by delivering files requested by users when they enter a domain name in their browser’s address bar.

This tutorial is about installing and configuring Apache2 on your Ubuntu system. The commands and procedures mentioned in this article have been run on an Ubuntu 18.04 LTS system. Since we are using the Ubuntu command line, the Terminal, in this article; you can open it through the system Dash or the Ctrl+Alt+T shortcut.
Install Apache 2 on Ubuntu Linux

Please follow the following steps in order to install the Apache2 software through Ubuntu official repositories.

Step 1: Update system repositories

You can download the latest version of a software by first updating the local package index of Ubuntu repositories. Open the Terminal and enter the following command in order to do so:

$ sudo apt update

Step 2: Install Apache 2 with the apt command

sudo apt install apache2

Step 3: Verify the Apache installation

$ apache2 -version

thanks 



by farisy
